import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HelloWorldService { 
    constructor(private http: HttpClient) {

     }
     getWelcomeMessage() {
         return this.http.get('http://localhost:8080/api/v1/helloWorld');
     }
}