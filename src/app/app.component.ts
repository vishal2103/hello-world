import { Component } from '@angular/core';
import { HelloWorldService } from './helloworld.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  message: string;
  constructor(private service: HelloWorldService) {
    this.getWelcomeMessage();
  }
  getWelcomeMessage() {
    this.service.getWelcomeMessage().subscribe(res => {
      this.message = JSON.parse(JSON.stringify(res));
    })
  }
   title = 'HelloWorld';
}
