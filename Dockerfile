### STAGE 1: Build ###
FROM node:12.7-alpine AS build
WORKDIR /app
COPY package.json ./
RUN npm install
COPY . .
RUN npm run build
### STAGE 2: Run ###
FROM nginx
EXPOSE 80
COPY --from=build /app/dist/HelloWorld /usr/share/nginx/html
